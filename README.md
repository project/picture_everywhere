# Picture Everywhere

Picture Everywhere provides a plug and play override for image templates and
preprocessing to enforce use of the HTML &lt;picture&gt; tag everywhere, as
opposed to standalone &lt;img&gt; tags.


## Requirements

This module depends on the Drupal core Image module.


## Integrations

This module optionally integrates with these other modules:


### SVG Image Field

If the [SVG Image Field module](https://www.drupal.org/project/svg_image_field)
is installed, this module automatically integrates with it, wrapping its
&lt;img&gt; tags the same as for standard Drupal image rendering.


### WebP

There is an optional enhancement meant to work together with the WebP module.
Picture Everywhere can add a .webp &lt;source&gt; for all JPEG and PNG files.
The WebP module is able to serve that source URL automatically.

This enhancement is disabled by default and can be enabled in Picture Everywhere
configuration.

If you don't have to support older browsers without WebP support, it is
recommended to leave this setting off and instead include WebP conversion in all
of your image styles.


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

1. Enable the module at Administration > Extend.
1. Enable the module in one or more themes, and optionally configure other
   settings, at Administration > Configuration > Media > Picture Everywhere.
