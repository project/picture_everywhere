<?php

namespace Drupal\picture_everywhere\EventSubscriber;

use Drupal\Core\Cache\CacheTagsInvalidatorInterface;
use Drupal\Core\Config\ConfigCrudEvent;
use Drupal\Core\Config\ConfigEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Reacts to changes in this module's config.
 */
class ConfigSubscriber implements EventSubscriberInterface {

  /**
   * Constructs the subscriber.
   */
  public function __construct(
    protected CacheTagsInvalidatorInterface $cacheTagsInvalidator,
  ) {
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [
      ConfigEvents::SAVE   => 'configAltered',
      ConfigEvents::DELETE => 'configAltered',
    ];
  }

  /**
   * Invalidate cache on save.
   */
  public function configAltered(ConfigCrudEvent $event) {
    if ($event->getConfig()->getName() === 'picture_everywhere.settings') {
      $this->cacheTagsInvalidator->invalidateTags([
        'rendered',
        'theme_registry',
      ]);
    }
  }

}
