<?php

declare(strict_types=1);

namespace Drupal\Tests\picture_everywhere\Kernel;

use Drupal\responsive_image\Entity\ResponsiveImageStyle;

/**
 * Tests responsive image theme functions and output.
 *
 * @group picture_everywhere
 */
final class ResponsiveImageTest extends ImageTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'breakpoint',
    'field',
    'file',
    'image',
    'responsive_image',
    'responsive_image_test_module',
  ];

  /**
   * Tests that responsive_image output is not messed up by this module.
   */
  public function testResponsiveImageMarkup(): void {
    ResponsiveImageStyle::create([
      'id' => 'foo',
      'label' => 'Foo',
      'breakpoint_group' => 'responsive_image_test_module',
    ])->save();

    $this->enableForTheme(self::TEST_THEME);
    $this->setDefaultTheme(self::TEST_THEME);

    $image = [
      '#theme' => 'responsive_image',
      '#uri' => self::TEST_IMAGES[0],
      '#responsive_image_style_id' => 'foo',
    ];
    $this->render($image);

    $picture = $this->xpath('//picture');
    $this->assertCount(1, $picture, '<picture> tag is present and not duplicated when using picture_everywhere with responsive_image.');
    $picture = reset($picture);
    $img = $picture->xpath('//img');
    $this->assertCount(1, $img, '<img> tag is present and not duplicated when using picture_everywhere with responsive_image.');
  }

}
