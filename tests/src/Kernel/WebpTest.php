<?php

declare(strict_types=1);

namespace Drupal\Tests\picture_everywhere\Kernel;

/**
 * Tests Webp module integration.
 *
 * @group picture_everywhere
 */
final class WebpTest extends ImageTestBase {

  /**
   * Tests that a <picture> tag is used for image output.
   */
  public function testWebpAutomaticSourceEnabled(): void {
    $this->enableForTheme(self::TEST_THEME);
    $this->setDefaultTheme(self::TEST_THEME);
    $this->enableAutoWebpSource();

    $test_image_uri = self::TEST_IMAGES[0];

    $image = [
      '#theme' => 'image',
      '#uri' => $test_image_uri,
    ];
    $this->render($image);

    $picture = $this->xpath('//picture');
    $picture = reset($picture);
    $source = $picture->xpath('//source');
    $this->assertCount(1, $source, 'Enabling automatic webp source should create a single <source> tag.');
    $source = reset($source);

    $expected_source = $this->fileUrlGenerator->generateString($test_image_uri);
    $expected_source = preg_replace('/\..{3,4}$/i', '.webp', $expected_source, 1, $replacement_count);
    $this->assertSame(1, $replacement_count);

    $this->assertSame($expected_source, $source->attributes()->srcset->__toString());
  }

  /**
   * Tests that webp automatic source is not created when option not enabled.
   */
  public function testWebpAutomaticSourceNotEnabled(): void {
    $this->enableForTheme(self::TEST_THEME);
    $this->setDefaultTheme(self::TEST_THEME);

    $image = [
      '#theme' => 'image',
      '#uri' => self::TEST_IMAGES[0],
    ];
    $this->render($image);

    $picture = $this->xpath('//picture');
    $picture = reset($picture);
    $this->assertCount(0, $picture->xpath('//source'), 'No <source> tags should be created for a basic image when automatic source creation is not enabled.');
  }

  /**
   * Enables the "Automatically add a Webp source" setting.
   */
  protected function enableAutoWebpSource(): void {
    $this->config('picture_everywhere.settings')
      ->set('output.webp.auto_webp_source', TRUE)
      ->save();
  }

}
