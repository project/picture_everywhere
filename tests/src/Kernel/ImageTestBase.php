<?php

declare(strict_types=1);

namespace Drupal\Tests\picture_everywhere\Kernel;

use Drupal\Core\Extension\ThemeInstallerInterface;
use Drupal\Core\File\FileUrlGeneratorInterface;
use Drupal\KernelTests\KernelTestBase;

/**
 * Base class for testing theme functions and output.
 */
abstract class ImageTestBase extends KernelTestBase {

  /**
   * The name of the theme tests will run under.
   */
  public const TEST_THEME = 'stark';

  /**
   * The name of another to use for comparison in tests.
   */
  public const UNTESTED_THEME = 'olivero';

  /**
   * URIs of test images.
   *
   * Copied from \Drupal\KernelTests\Core\Theme\ImageTest.
   */
  public const TEST_IMAGES = [
    'core/misc/druplicon.png',
    'core/misc/loading.gif',
  ];

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'picture_everywhere',
    'system',
  ];

  /**
   * The file URL generator.
   */
  protected FileUrlGeneratorInterface $fileUrlGenerator;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->fileUrlGenerator = $this->container->get('file_url_generator');

    $theme_installer = \Drupal::service('theme_installer');
    assert($theme_installer instanceof ThemeInstallerInterface);
    $theme_installer->install([
      static::TEST_THEME,
      static::UNTESTED_THEME,
    ]);

    $this->installConfig('picture_everywhere');
  }

  /**
   * Enables Picture Everywhere functionality for a theme.
   */
  protected function enableForTheme(string $theme) {
    if (!$theme) {
      throw new \RuntimeException('enableForTheme() must be provided a truthy value.');
    }

    $picture_everywhere_config = $this->config('picture_everywhere.settings');
    $enabled_themes = $picture_everywhere_config->get('themes') ?: [];
    if (!in_array($theme, $enabled_themes)) {
      $enabled_themes[] = $theme;
    }
    $picture_everywhere_config
      ->set('themes', $enabled_themes)
      ->save();
  }

  /**
   * Sets the default Drupal theme.
   */
  protected function setDefaultTheme(string $theme): void {
    $this->config('system.theme')
      ->set('default', $theme)
      ->save();
  }

}
