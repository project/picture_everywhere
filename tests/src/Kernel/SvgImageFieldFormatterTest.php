<?php

declare(strict_types=1);

namespace Drupal\Tests\picture_everywhere\Kernel;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\file\Entity\File;
use Drupal\file\FileInterface;
use Drupal\node\Entity\Node;
use Drupal\node\Entity\NodeType;
use Drupal\Tests\svg_image_field\Traits\SvgImageFieldCommonTrait;
use Drupal\Tests\svg_image_field\Traits\SvgImageFieldCreationTrait;

/**
 * Tests image theme functions and output when svg_image_field is installed.
 *
 * @group picture_everywhere
 */
final class SvgImageFieldFormatterTest extends ImageTestBase {

  use SvgImageFieldCommonTrait;
  use SvgImageFieldCreationTrait;

  public const NODE_TYPE_ID = 'test_type';

  public const SVG_FIELD_NAME = 'field_svg';

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'field',
    'file',
    'image',
    'node',
    'svg_image_field',
    'user',
  ];

  /**
   * A test entity prepared for rendering by setUp().
   */
  protected ContentEntityInterface $testEntity;

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();

    $this->installEntitySchema('user');

    $this->installEntitySchema('file');
    $this->installSchema('file', ['file_usage']);
    $this->installEntitySchema('file');

    $this->installEntitySchema('node');

    NodeType::create([
      'type' => self::NODE_TYPE_ID,
      'name' => self::NODE_TYPE_ID,
    ])
      ->save();

    $this->createSvgImageField(self::SVG_FIELD_NAME, self::NODE_TYPE_ID, ['uri_scheme' => 'public']);

    $files = scandir($this->resourcesPath());
    $file = NULL;
    foreach ($files as $file_name) {
      if (str_starts_with($file_name, 'valid_svg')) {
        $file_path = realpath($this->resourcesPath() . '/' . $file_name);
        $file = File::create([
          'uri' => $file_path,
        ]);
        $file->save();
        break;
      }
    }
    assert($file instanceof FileInterface);

    $this->testEntity = Node::create([
      'type' => self::NODE_TYPE_ID,
      'title' => 'SVG test node',
      'field_svg' => [
        'target_id' => $file->id(),
      ],
    ]);
    $this->testEntity->save();
  }

  /**
   * Tests that a <picture> tag is used for SVG output when theme is enabled.
   */
  public function testSvgPictureTagInEnabledTheme(): void {
    $this->enableForTheme(self::TEST_THEME);
    $this->setDefaultTheme(self::TEST_THEME);

    $field_view = $this->testEntity->get(self::SVG_FIELD_NAME)->view();
    $this->render($field_view);

    $picture = $this->xpath('//picture');
    $this->assertCount(1, $picture, 'Picture tag is in use for svg_image_field formatter when current theme is enabled for Picture Everywhere.');
    $picture = reset($picture);
    $img = $picture->xpath('//img');
    $this->assertCount(1, $img, '<img> tag is present and not duplicated for svg_image_field formatter when current theme is enabled for Picture Everywhere.');
  }

  /**
   * Tests <picture> tag is not used for image output when theme is disabled.
   */
  public function testSvgPictureTagInDisabledTheme(): void {
    $this->enableForTheme(self::UNTESTED_THEME);
    $this->setDefaultTheme(self::TEST_THEME);

    $field_view = $this->testEntity->get(self::SVG_FIELD_NAME)->view();
    $this->render($field_view);

    $this->assertCount(0, $this->xpath('//picture'), 'Picture tag is not in use for svg_image_field formatter when current theme is not enabled for Picture Everywhere.');
  }

}
