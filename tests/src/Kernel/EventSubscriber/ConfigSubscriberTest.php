<?php

declare(strict_types=1);

namespace Drupal\Tests\picture_everywhere\Kernel\EventSubscriber;

use Drupal\Core\Cache\CacheTagsInvalidator;
use Drupal\KernelTests\KernelTestBase;

/**
 * @coversDefaultClass \Drupal\picture_everywhere\EventSubscriber\ConfigSubscriber
 *
 * @group picture_everywhere
 */
final class ConfigSubscriberTest extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'picture_everywhere',
  ];

  /**
   * Tests the configAltered() method.
   */
  public function testConfigAltered(): void {
    $picture_everywhere_config = $this->config('picture_everywhere.settings');
    $config_cache_tags = $picture_everywhere_config->getCacheTags();

    $mock_invalidator = $this->createMock(CacheTagsInvalidator::class);
    $mock_invalidator->expects(self::atLeast(2))
      ->method('invalidateTags')
      ->willReturnCallback(function (array $tags) use ($config_cache_tags): void {
        // We expect the cache tag(s) of the config itself to be invalidated in
        // a separate call from Core code, so we don't want to run assertions on
        // tag groups containing them.
        foreach ($config_cache_tags as $config_cache_tag) {
          if (in_array($config_cache_tag, $tags, TRUE)) {
            return;
          }
        }

        self::assertContains('rendered', $tags);
        self::assertContains('theme_registry', $tags);
      });
    \Drupal::service('cache_tags.invalidator')->addInvalidator($mock_invalidator);

    $picture_everywhere_config->save();

    $picture_everywhere_config->delete();
  }

}
