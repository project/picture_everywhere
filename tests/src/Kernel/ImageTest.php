<?php

declare(strict_types=1);

namespace Drupal\Tests\picture_everywhere\Kernel;

use Drupal\picture_everywhere\Preprocess;

/**
 * Tests image theme functions and output.
 *
 * @group picture_everywhere
 */
final class ImageTest extends ImageTestBase {

  /**
   * Tests that a <picture> tag is used for image output.
   */
  public function testPictureTagInEnabledTheme(): void {
    $this->enableForTheme(self::TEST_THEME);
    $this->setDefaultTheme(self::TEST_THEME);

    $image = [
      '#theme' => 'image',
      '#uri' => self::TEST_IMAGES[0],
    ];
    $this->render($image);
    $pictures = $this->xpath('//picture');
    $this->assertCount(1, $pictures, 'Picture tag is in use when current theme is enabled for Picture Everywhere.');
  }

  /**
   * Tests that <picture> is not used when a theme is not enabled in settings.
   */
  public function testPictureTagInDisabledTheme(): void {
    $this->enableForTheme(self::UNTESTED_THEME);
    $this->setDefaultTheme(self::TEST_THEME);

    $image = [
      '#theme' => 'image',
      '#uri' => self::TEST_IMAGES[0],
    ];
    $this->render($image);
    $pictures = $this->xpath('//picture');
    $this->assertCount(0, $pictures, 'Picture tag is not in use when current theme is not enabled for Picture Everywhere.');
  }

  /**
   * Tests that <picture> is not used when a theme is not enabled in settings.
   */
  public function testPictureTagWithoutEnabledThemes(): void {
    // Intentionally use our traditionally enabled theme - it should not matter
    // if none are set to enabled.
    $this->setDefaultTheme(self::TEST_THEME);

    $image = [
      '#theme' => 'image',
      '#uri' => self::TEST_IMAGES[0],
    ];
    $this->render($image);
    $pictures = $this->xpath('//picture');
    $this->assertCount(0, $pictures, 'Picture tag is not in use when no themes are enabled.');
  }

  /**
   * Tests that attributes meant to always go on the <img>, appear there.
   */
  public function testBuiltInImageAttributes(): void {
    $this->enableForTheme(self::TEST_THEME);
    $this->setDefaultTheme(self::TEST_THEME);

    $test_image_uri = self::TEST_IMAGES[0];

    $image = [
      '#theme' => 'image',
      '#uri' => $test_image_uri,
    ];
    $attribute_values = [];
    foreach (Preprocess::ALWAYS_IMG_TAG_ATTRS as $attribute_name) {
      $attribute_values[$attribute_name] = match($attribute_name) {
        'src' => $this->fileUrlGenerator->generateString($test_image_uri),
        default => $this->randomString(8),
      };

      if (!in_array($attribute_name, ['src'], TRUE)) {
        $image['#attributes'][$attribute_name] = $attribute_values[$attribute_name];
      }
    }
    $this->render($image);

    $picture = $this->xpath('//picture');
    $this->assertCount(1, $picture, 'Render output contains only one <picture>.');
    $picture = reset($picture);
    $img = $picture->xpath('//img');
    $this->assertCount(1, $img, '<picture> tag contains only one <img>.');
    $img = reset($img);

    foreach (Preprocess::ALWAYS_IMG_TAG_ATTRS as $attribute_name) {
      $this->assertSame($attribute_values[$attribute_name], $img->attributes()->{$attribute_name}?->__toString(), "Attribute '$attribute_name' should always be on the <img> tag.");
      $this->assertNull($picture->attributes()->{$attribute_name}, "Attribute '$attribute_name' should never be on the <picture> tag.");
    }
  }

  /**
   * Tests that attributes only go on the <img> tag if configured.
   */
  public function testCustomImageAttributes(): void {
    $this->enableForTheme(self::TEST_THEME);
    $this->setDefaultTheme(self::TEST_THEME);

    $test_img_attribute_names = [
      'data-arbitrary',
      'groucho',
    ];
    $test_picture_attribute_names = [
      'class',
      'data-random',
    ];

    $picture_everywhere_config = $this->config('picture_everywhere.settings');
    $picture_everywhere_config
      ->set('output.img_tag_attributes', $test_img_attribute_names)
      ->save();

    $image = [
      '#theme' => 'image',
      '#uri' => self::TEST_IMAGES[0],
    ];
    $attribute_values = [];
    foreach (array_merge($test_img_attribute_names, $test_picture_attribute_names) as $attribute_name) {
      $attribute_values[$attribute_name] = $this->randomString(8);
      $image['#attributes'][$attribute_name] = $attribute_values[$attribute_name];
    }
    $this->render($image);

    $picture = $this->xpath('//picture');
    $this->assertCount(1, $picture, 'Render output contains only one <picture>.');
    $picture = reset($picture);
    $img = $picture->xpath('//img');
    $this->assertCount(1, $img, '<picture> tag contains only one <img>.');
    $img = reset($img);

    foreach ($test_img_attribute_names as $attribute_name) {
      $this->assertSame($attribute_values[$attribute_name], $img->attributes()->{$attribute_name}?->__toString(), "Custom attribute '$attribute_name' should be on the <img> tag.");
      $this->assertNull($picture->attributes()->{$attribute_name}, "Custom attribute '$attribute_name' should not be on the <picture> tag.");
    }

    foreach ($test_picture_attribute_names as $attribute_name) {
      $this->assertSame($attribute_values[$attribute_name], $picture->attributes()->{$attribute_name}?->__toString(), "Custom attribute '$attribute_name' should be on the <picture> tag since it has not been configured otherwise.");
      $this->assertNull($img->attributes()->{$attribute_name}, "Custom attribute '$attribute_name' should not be on the <img> tag since it has not been configured to be.");
    }
  }

}
