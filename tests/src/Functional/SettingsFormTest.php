<?php

declare(strict_types=1);

namespace Drupal\Tests\picture_everywhere\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Tests Picture Everywhere's settings form.
 *
 * @group picture_everywhere
 */
final class SettingsFormTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'picture_everywhere',
  ];

  /**
   * {@inheritdoc}
   */
  protected $profile = 'testing';

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Tests the module's settings form.
   */
  public function testForm(): void {
    $admin_user = $this->drupalCreateUser([
      'administer site configuration',
    ]);
    $this->drupalLogin($admin_user);

    // Save an initial set of values.
    $this->drupalGet('admin/config/media/picture-everywhere');
    $this->submitForm([
      'themes[stark]' => TRUE,
      'output[img_tag_attributes]' => "data-my-attribute\ndata-nifty",
      'output[webp][auto_webp_source]' => FALSE,
    ], 'Save configuration');

    // Check that config was saved as expected.
    $config = $this->config('picture_everywhere.settings');
    $this->assertEquals([$this->defaultTheme], $config->get('themes'), 'Selected themes should be saved.');
    $this->assertEquals(['data-my-attribute', 'data-nifty'], $config->get('output.img_tag_attributes'), 'Custom <img> tag attributes should be saved.');

    // Check that saved values are displayed on form as expected.
    $this->drupalGet('admin/config/media/picture-everywhere');
    $this->assertSession()->checkboxChecked('themes[stark]');
    $this->assertSession()->fieldValueEquals('output[img_tag_attributes]', "data-my-attribute\ndata-nifty");
    $this->assertSession()->checkboxNotChecked('output[webp][auto_webp_source]');

    // Save another set of values.
    $this->submitForm([
      'themes[stark]' => FALSE,
      'output[img_tag_attributes]' => '',
      'output[webp][auto_webp_source]' => TRUE,
    ], 'Save configuration');

    // Check the new set of values.
    $this->drupalGet('admin/config/media/picture-everywhere');
    $this->assertSession()->checkboxNotChecked('themes[stark]');
    $this->assertSession()->fieldValueEquals('output[img_tag_attributes]', '');
    $this->assertSession()->checkboxChecked('output[webp][auto_webp_source]');
  }

}
